/*
 *  Copyright (C) 2022  MURENA SAS
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.apps

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.aurora.gplayapi.data.models.AuthData
import foundation.e.apps.data.blockedApps.BlockedAppRepository
import foundation.e.apps.data.enums.FilterLevel
import foundation.e.apps.data.enums.ResultStatus
import foundation.e.apps.data.enums.Status
import foundation.e.apps.data.faultyApps.FaultyAppRepository
import foundation.e.apps.data.fdroid.FDroidRepository
import foundation.e.apps.data.application.ApplicationRepository
import foundation.e.apps.data.application.search.SearchApi
import foundation.e.apps.data.application.data.Application
import foundation.e.apps.data.enums.Source
import foundation.e.apps.data.gitlab.SystemAppsUpdatesRepository
import foundation.e.apps.data.updates.UpdatesManagerImpl
import foundation.e.apps.util.MainCoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import org.mockito.kotlin.eq

@OptIn(ExperimentalCoroutinesApi::class)
class UpdateManagerImptTest {

    // Run tasks synchronously
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    // Sets the main coroutines dispatcher to a TestCoroutineScope for unit testing.
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var updatesManagerImpl: UpdatesManagerImpl

    @Mock
    private lateinit var context: Context

    private lateinit var pkgManagerModule: FakeAppLoungePackageManager

    @Mock
    private lateinit var applicationRepository: ApplicationRepository

    private lateinit var preferenceModule: FakeAppLoungePreference

    private lateinit var faultyAppRepository: FaultyAppRepository

    @Mock
    private lateinit var blockedAppRepository: BlockedAppRepository

    @Mock
    private lateinit var fDroidRepository: FDroidRepository

    @Mock
    private lateinit var systemAppsUpdatesRepository: SystemAppsUpdatesRepository

    val authData = AuthData("e@e.email", "AtadyMsIAtadyM")

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        faultyAppRepository = FaultyAppRepository(FakeFaultyAppDao())
        preferenceModule = FakeAppLoungePreference(context)
        pkgManagerModule = FakeAppLoungePackageManager(context, getGplayApps())
        updatesManagerImpl = UpdatesManagerImpl(
            context,
            pkgManagerModule,
            applicationRepository,
            faultyAppRepository,
            preferenceModule,
            fDroidRepository,
            blockedAppRepository,
            systemAppsUpdatesRepository,
        )
    }

    private fun getSystemApps(status: Status = Status.UPDATABLE) = mutableListOf(
        Application(
            status = status,
            name = "Demo Four",
            package_name = "foundation.e.demofour",
            source = Source.SYSTEM_APP,
            filterLevel = FilterLevel.NONE
        )
    )

    @Test
    fun getUpdateWhenUpdateIsAvailable() = runTest {
        val gplayApps = getGplayApps()
        val openSourceApps = getOpenSourceApps(Status.UPDATABLE)
        val systemAppUpdates = getSystemApps()

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.OK)
        val gplayUpdates = Pair(gplayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(
            openSourceUpdates,
            gplayUpdates,
            systemAppUpdates
        )

        val updateResult = updatesManagerImpl.getUpdates()
        System.out.println("===> updates: ${updateResult.first.map { it.package_name }}")

        assertEquals("fetchUpdate", 3, updateResult.first.size)
    }

    private fun getGplayApps(status: Status = Status.UPDATABLE) = mutableListOf<Application>(
        Application(
            _id = "111",
            status = status,
            name = "Demo One",
            package_name = "foundation.e.demoone",
            source = Source.PLAY_STORE,
            filterLevel = FilterLevel.NONE
        ),
        Application(
            _id = "112",
            status = Status.INSTALLED,
            name = "Demo Two",
            package_name = "foundation.e.demotwo",
            source = Source.PLAY_STORE,
            filterLevel = FilterLevel.NONE
        ),
    )

    @Test
    fun getUpdateWhenInstalledPackageListIsEmpty() = runTest {
        val authData = AuthData("e@e.email", "AtadyMsIAtadyM")
        pkgManagerModule.applicationInfo.clear()

        setupMockingSystemApps()

        val updateResult = updatesManagerImpl.getUpdates()
        System.out.println("===> updates: ${updateResult.first.map { it.package_name }}")

        assertEquals("fetchUpdate", 0, updateResult.first.size)
    }

    @Test
    fun getUpdateWhenUpdateIsUnavailable() = runTest {
        val gplayApps = getGplayApps(Status.INSTALLED)
        val openSourceApps = getOpenSourceApps(Status.INSTALLED)
        val systemAppUpdates = getSystemApps(Status.INSTALLED)

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.OK)
        val gplayUpdates = Pair(gplayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(
            openSourceUpdates,
            gplayUpdates,
            systemAppUpdates,
        )

        val updateResult = updatesManagerImpl.getUpdates()
        System.out.println("===> updates: ${updateResult.first.map { it.package_name }}")

        assertEquals("fetchUpdate", 0, updateResult.first.size)
    }

    @Test
    fun getUpdateWhenUpdateHasOnlyForOpenSourceApps() = runTest {
        val gplayApps = getGplayApps(Status.INSTALLED)
        val openSourceApps = getOpenSourceApps(Status.UPDATABLE)
        val systemAppUpdates = getSystemApps(Status.INSTALLED)

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.OK)
        val gplayUpdates = Pair(gplayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(
            openSourceUpdates,
            gplayUpdates,
            systemAppUpdates,
        )

        val updateResult = updatesManagerImpl.getUpdates()
        System.out.println("===> updates: ${updateResult.first.map { it.package_name }}")

        assertFalse("fetchupdate", updateResult.first.any { it.source != Source.OPEN_SOURCE && it.source != Source.PWA })
    }

    @Test
    fun getUpdateWhenUpdateHasOnlyForGplayApps() = runTest {
        val gplayApps = getGplayApps(Status.UPDATABLE)
        val openSourceApps = getOpenSourceApps(Status.INSTALLED)
        val systemAppUpdates = getSystemApps(Status.INSTALLED)

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.OK)
        val gplayUpdates = Pair(gplayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(
            openSourceUpdates,
            gplayUpdates,
            systemAppUpdates,
        )

        val updateResult = updatesManagerImpl.getUpdates()
        assertFalse("fetchupdate", updateResult.first.any { it.source != Source.PLAY_STORE })
    }

    @Test
    fun getUpdateWhenUpdateHasOnlySystemApps() = runTest {
        val gplayApps = getGplayApps(Status.INSTALLED)
        val openSourceApps = getOpenSourceApps(Status.INSTALLED)
        val systemAppUpdates = getSystemApps(Status.UPDATABLE)

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.OK)
        val gplayUpdates = Pair(gplayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(
            openSourceUpdates,
            gplayUpdates,
            systemAppUpdates,
        )

        val updateResult = updatesManagerImpl.getUpdates()
        assertFalse("fetchupdate", updateResult.first.any { it.source != Source.SYSTEM_APP })
    }

    @Test
    fun getUpdateWhenFetchingOpenSourceIsFailed() = runTest {
        val gplayApps = getGplayApps(Status.UPDATABLE)
        val openSourceApps = mutableListOf<Application>()

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.TIMEOUT)
        val gplayUpdates = Pair(gplayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(
            openSourceUpdates,
            gplayUpdates
        )

        val updateResult = updatesManagerImpl.getUpdates()
        assertEquals("fetchupdate", 1, updateResult.first.size)
        assertEquals("fetchupdate", ResultStatus.OK, updateResult.second)
    }

    @Test
    fun getUpdateWhenFetchingGplayIsFailed() = runTest {
        val gplayApps = mutableListOf<Application>()
        val openSourceApps = getOpenSourceApps(Status.UPDATABLE)

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.OK)
        val gplayUpdates = Pair(gplayApps, ResultStatus.TIMEOUT)

        setupMockingForFetchingUpdates(
            openSourceUpdates,
            gplayUpdates
        )

        val updateResult = updatesManagerImpl.getUpdates()
        System.out.println("===> updates: ${updateResult.first.map { it.package_name }}")

        assertEquals("fetchupdate", 1, updateResult.first.size)
        assertEquals("fetchupdate", ResultStatus.OK, updateResult.second)
    }

    @Test
    fun getUpdateWhenBothSourcesAreFailed() = runTest {
        val gplayApps = mutableListOf<Application>()
        val openSourceApps = getOpenSourceApps(Status.UPDATABLE)

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.TIMEOUT)
        val gplayUpdates = Pair(gplayApps, ResultStatus.TIMEOUT)

        setupMockingForFetchingUpdates(
            openSourceUpdates,
            gplayUpdates
        )

        val updateResult = updatesManagerImpl.getUpdates()
        System.out.println("===> updates: ${updateResult.first.map { it.package_name }}")

        assertEquals("fetchupdate", 1, updateResult.first.size)
        assertEquals("fetchupdate", ResultStatus.TIMEOUT, updateResult.second)
    }

    private fun getOpenSourceApps(status: Status = Status.UPDATABLE) = mutableListOf<Application>(
        Application(
            _id = "113",
            status = status,
            name = "Demo Three",
            package_name = "foundation.e.demothree",
            source = Source.OPEN_SOURCE,
            filterLevel = FilterLevel.NONE
        )
    )

    @Test
    fun getUpdatesOSSWhenUpdateIsAvailable() = runTest {
        val openSourceApps = getOpenSourceApps(Status.UPDATABLE)
        val gPlayApps = getGplayApps(Status.UPDATABLE)
        val systemAppUpdates = getSystemApps()

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.OK)
        val gplayUpdates = Pair(gPlayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(openSourceUpdates, gplayUpdates, systemAppUpdates)

        val updateResult = updatesManagerImpl.getUpdatesOSS()
        assertEquals("UpdateOSS", 2, updateResult.first.size)
        assertEquals("UpdateOSS", Source.OPEN_SOURCE, updateResult.first[1].source)
        assertEquals("UpdateOSS", Source.SYSTEM_APP, updateResult.first[0].source)
    }

    @Test
    fun getUpdatesOSSWhenUpdateIsUnavailable() = runTest {
        val openSourceApps = getOpenSourceApps(Status.INSTALLED)
        val gPlayApps = getGplayApps(Status.UPDATABLE)

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.OK)
        val gplayUpdates = Pair(gPlayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(openSourceUpdates, gplayUpdates)

        val updateResult = updatesManagerImpl.getUpdatesOSS()
        assertEquals("UpdateOSS", 0, updateResult.first.size)
    }

    @Test
    fun getUpdatesOSSWhenOpenSourceIsFailed() = runTest {
        val openSourceApps = mutableListOf<Application>()
        val gPlayApps = getGplayApps(Status.UPDATABLE)

        val openSourceUpdates = Pair(openSourceApps, ResultStatus.TIMEOUT)
        val gplayUpdates = Pair(gPlayApps, ResultStatus.OK)

        setupMockingForFetchingUpdates(openSourceUpdates, gplayUpdates)

        val updateResult = updatesManagerImpl.getUpdatesOSS()
        assertEquals("UpdateOSS", 0, updateResult.first.size)
        assertEquals("UpdateOSS", ResultStatus.TIMEOUT, updateResult.second)
    }

    private suspend fun setupMockingForFetchingUpdates(
        openSourceUpdates: Pair<MutableList<Application>, ResultStatus>,
        gplayUpdates: Pair<MutableList<Application>, ResultStatus>,
        systemAppUpdates: MutableList<Application> = mutableListOf(),
        selectedApplicationSources: List<String> = mutableListOf(
            SearchApi.APP_TYPE_ANY,
            SearchApi.APP_TYPE_OPEN,
            SearchApi.APP_TYPE_PWA
        )
    ) {
        Mockito.`when`(
            applicationRepository.getApplicationDetails(
                any(),
                eq(Source.OPEN_SOURCE)
            )
        ).thenReturn(openSourceUpdates)

        Mockito.`when`(applicationRepository.getSelectedAppTypes())
            .thenReturn(selectedApplicationSources)

        setupMockingSystemApps(systemAppUpdates)

        if (gplayUpdates.first.isNotEmpty()) {
            Mockito.`when`(
                applicationRepository.getApplicationDetails(
                    any(),
                    any(),
                    eq(Source.PLAY_STORE)
                )
            ).thenReturn(
                Pair(gplayUpdates.first.first(), ResultStatus.OK),
                Pair(gplayUpdates.first[1], ResultStatus.OK)
            )
        } else {
            Mockito.`when`(
                applicationRepository.getApplicationDetails(
                    any(),
                    any(),
                    eq(Source.PLAY_STORE)
                )
            ).thenReturn(Pair(Application(), ResultStatus.TIMEOUT))
        }
    }

    private suspend fun setupMockingSystemApps(
        systemAppUpdates: MutableList<Application> = mutableListOf()
    ) {
        Mockito.`when`(systemAppsUpdatesRepository.getSystemUpdates())
            .thenReturn(systemAppUpdates)
    }
}
