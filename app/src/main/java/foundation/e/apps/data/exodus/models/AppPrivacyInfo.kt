package foundation.e.apps.data.exodus.models

data class AppPrivacyInfo(val numberOfTrackers: Int = 0, val numberOfPermissions: Int = 0, val reportId: Long = -1L)
