/*
 * Apps  Quickly and easily install Android apps onto your device!
 * Copyright (C) 2021  E FOUNDATION
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package foundation.e.apps.ui.search

import androidx.annotation.GuardedBy
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.aurora.gplayapi.SearchSuggestEntry
import dagger.hilt.android.lifecycle.HiltViewModel
import foundation.e.apps.data.ResultSupreme
import foundation.e.apps.data.application.ApplicationRepository
import foundation.e.apps.data.application.data.Application
import foundation.e.apps.data.application.search.SearchResult
import foundation.e.apps.data.enums.Source
import foundation.e.apps.data.exodus.repositories.IAppPrivacyInfoRepository
import foundation.e.apps.data.exodus.repositories.PrivacyScoreRepository
import foundation.e.apps.data.login.AuthObject
import foundation.e.apps.ui.parentFragment.LoadingViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(
    private val applicationRepository: ApplicationRepository,
    private val privacyScoreRepository: PrivacyScoreRepository,
    private val appPrivacyInfoRepository: IAppPrivacyInfoRepository
) : LoadingViewModel() {

    val searchSuggest: MutableLiveData<List<SearchSuggestEntry>?> = MutableLiveData()

    private val _searchResult: MutableLiveData<SearchResult> =
        MutableLiveData()
    val searchResult: LiveData<SearchResult> = _searchResult

    private var lastAuthObjects: List<AuthObject>? = null

    private var isLoading: Boolean = false

    @GuardedBy("mutex")
    private val accumulatedList = mutableListOf<Application>()
    private val mutex = Mutex()

    private var flagNoTrackers: Boolean = false
    private var flagOpenSource: Boolean = false
    private var flagPWA: Boolean = false

    companion object {
        private const val PREVENT_HTTP_429_DELAY_IN_MS = 1000L
    }

    fun setFilterFlags(
        flagNoTrackers: Boolean = false,
        flagOpenSource: Boolean = false,
        flagPWA: Boolean = false,
    ) {
        this.flagNoTrackers = flagNoTrackers
        this.flagOpenSource = flagOpenSource
        this.flagPWA = flagPWA

        viewModelScope.launch(IO) {
            emitFilteredResults(null)
        }
    }

    fun getSearchSuggestions(query: String) {
        viewModelScope.launch(IO) {
            searchSuggest.postValue(
                applicationRepository.getSearchSuggestions(query)
            )
        }
    }

    fun loadData(
        query: String,
        authObjects: List<AuthObject>,
        retryBlock: (failedObjects: List<AuthObject>) -> Boolean
    ) {
        if (query.isBlank()) return

        this.lastAuthObjects = authObjects
        super.onLoadData(authObjects, { successObjects, failedObjects ->
            viewModelScope.launch {
                mutex.withLock {
                    accumulatedList.clear()
                }
            }

            successObjects.find { it is AuthObject.CleanApk }?.run {
                fetchCleanApkData(query)
            }

            successObjects.find { it is AuthObject.GPlayAuth }?.run {
                fetchGplayData(query)
            }

            failedObjects.find { it is AuthObject.GPlayAuth }?.run {
                fetchGplayData(query)
            }

        }, retryBlock)
    }

    /*
     * Observe data from Fused API and publish the result in searchResult.
     * This allows us to show apps as they are being fetched from the network,
     * without having to wait for all of the apps.
     * Issue: https://gitlab.e.foundation/e/backlog/-/issues/5171
     */
    private fun fetchCleanApkData(
        query: String
    ) {
        viewModelScope.launch(IO) {
            val searchResultSupreme = applicationRepository.getCleanApkSearchResults(query)

            emitFilteredResults(searchResultSupreme)

            if (!searchResultSupreme.isSuccess()) {
                searchResultSupreme.exception?.let { handleException(it) }
            }
        }
    }

    fun loadMore(query: String, autoTriggered: Boolean = false) {
        viewModelScope.launch(Main) {
            if (isLoading) {
                Timber.d("Search result is loading....")
                return@launch
            }

            if (autoTriggered) {
                delay(PREVENT_HTTP_429_DELAY_IN_MS)
            }
            fetchGplayData(query)
        }
    }

    fun sortApps(apps: List<Application>): List<Application> {
        return apps.filter { it.name.isNotBlank() }.sortedBy { it.source }.distinctBy { it.package_name }
    }

    private fun fetchGplayData(query: String) {
        viewModelScope.launch(IO) {
            isLoading = true

            val gplaySearchResult =
                applicationRepository.getGplaySearchResults(query)

            if (!gplaySearchResult.isSuccess()) {
                gplaySearchResult.exception?.let {
                    handleException(it)
                }
            }

            val currentAppList = updateCurrentAppList(gplaySearchResult)

            val finalResult = ResultSupreme.Success(
                Pair(currentAppList.toList(), false)
            )

            emitFilteredResults(finalResult)

            isLoading = false
        }
    }

    private suspend fun updateCurrentAppList(searchResult: SearchResult): List<Application> {
        val currentAppList = mutex.withLock {
            accumulatedList
        }

        currentAppList.removeIf { item -> item.isPlaceHolder }
        currentAppList.addAll(searchResult.data?.first ?: emptyList())
        return currentAppList.distinctBy { it.package_name }
    }

    private fun handleException(exception: Exception) {
        exceptionsList.add(exception)
        exceptionsLiveData.postValue(exceptionsList)
    }

    /**
     * @return returns true if there is changes in data, otherwise false
     */
    fun isAnyAppUpdated(
        newApplications: List<Application>,
        oldApplications: List<Application>
    ) = applicationRepository.isAnyFusedAppUpdated(newApplications, oldApplications)

    fun isAuthObjectListSame(authObjectList: List<AuthObject>?): Boolean {
        return lastAuthObjects == authObjectList
    }

    private fun hasTrackers(app: Application): Boolean {
        return when {
            app.privacyScore == 0 -> true               // Manually blocked apps (Facebook etc.)
            else -> false
        }
    }

    private suspend fun fetchTrackersForApp(app: Application) {
        if (app.isPlaceHolder) return
        appPrivacyInfoRepository.getAppPrivacyInfo(app, app.package_name).let {
            val calculatedScore = privacyScoreRepository.calculatePrivacyScore(app)
            app.privacyScore = calculatedScore
        }
    }

    private suspend fun getFilteredList(): List<Application> = withContext(IO) {
        if (flagNoTrackers) {
            mutex.withLock {
                val deferredCheck = accumulatedList.map {
                    async {
                        if (it.privacyScore == -1) {
                            fetchTrackersForApp(it)
                        }
                        it
                    }
                }
                deferredCheck.awaitAll()
            }
        }

        mutex.withLock {
            accumulatedList.filter {
                if (!flagNoTrackers && !flagOpenSource && !flagPWA) return@filter true
                if (flagNoTrackers && !hasTrackers(it)) return@filter true
                if (flagOpenSource && !it.is_pwa && it.source == Source.OPEN_SOURCE) return@filter true
                if (flagPWA && it.is_pwa) return@filter true
                false
            }
        }
    }

    /**
     * Pass [result] as null to re-emit already loaded search results with new filters.
     */
    private suspend fun emitFilteredResults(
        result: SearchResult? = null
    ) {

        // When filters are changed but no data is fetched yet
        if (result == null && _searchResult.value == null) {
            return
        }

        if (result != null && !result.isSuccess()) {
            _searchResult.postValue(result!!)
            return
        }

        if (result != null) {
            result.data?.first?.let {
                mutex.withLock {
                    accumulatedList.addAll(it)
                }
            }
        }

        val filteredList = getFilteredList()

        _searchResult.postValue(
            ResultSupreme.Success(
                Pair(filteredList.toList(), false)
            )
        )
    }
}
